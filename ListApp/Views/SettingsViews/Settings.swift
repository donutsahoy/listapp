import StoreKit
import SwiftUI

struct Settings: View {
    @Environment(\.managedObjectContext) private var viewContext
    @EnvironmentObject var store: Store

    @State var isStapleAlertVisible: Bool = false
    @State var isStapleLoadSuccess: Bool = false

    var body: some View {
        VStack {
            ScrollView {
                NavigationLink("Categories", destination: SettingsCategoriesView())
                    .buttonStyle(.bordered)

                NavigationLink("Basket History", destination: SettingsBasketHistoryView(viewContext: viewContext))
                    .buttonStyle(.bordered)

                NavigationLink("Edit Pantry Staples", destination: SettingsEditStaplesView())
                    .buttonStyle(.bordered)
                    .padding(.top)

                Button("Load Pantry Staples", action: {
                    viewContext.refreshAllObjects()
                    isStapleLoadSuccess = ListItem.loadStaples(viewContext)
                    isStapleAlertVisible.toggle()
                })
                .buttonStyle(.bordered)
                .alert(isStapleLoadSuccess
                    ? "Loaded Staples successfully."
                    : "Issue encountered loading staples, please try again.", isPresented: $isStapleAlertVisible)
                {
                    Button("OK", role: .cancel) {}
                }
                .padding(.bottom)

                SettingsViewShareButtons()

                ContactSupport()

                NavigationLink("Legal Information", destination: SettingsLegalView())
                    .buttonStyle(.bordered)

                if let product = store.smallTipInAppPurchase {
                    SettingsInAppPurchases(product: product)
                        .padding(.top)
                }

                AppVersionView()

                HStack {
                    Spacer()
                }
            }

            HStack {
                Spacer()

                Button {
                    if let url = URL(string: "https://shiner.rjs-app-dev.us/") {
                        UIApplication.shared.open(url)
                    }
                } label: {
                    Image(systemName: "pawprint.circle")
                }
                .padding(.horizontal)
            }
        }
        .frame(
            maxWidth: .infinity,
            maxHeight: .infinity
        )
        .navigationBarTitleDisplayMode(.inline)
        .toolbar {
            ToolbarItem(placement: .principal) {
                HStack {
                    Text("Settings")
                        .font(.headline)
                        .foregroundColor(Color.Theme.seaGreen)
                }
            }
        }
        .background(Color.Theme.linen)
    }
}
