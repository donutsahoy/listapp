//
//  ShareListDto.swift
//  BasketBuddy
//
//  Created by Robert J. Sarvis Jr on 4/9/23.
//

import Foundation

struct ShareListDto: Codable {
    let itemsList: [ShareListItem]
}
