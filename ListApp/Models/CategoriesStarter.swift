//
//  Categories.swift
//  ListApp
//
//  Created by rjs on 12/29/21.
//

import Foundation

struct CategoriesStarter {
    let categoryNames = [
        "Produce",
        "Seafood",
        "Meat",
        "Dairy",
        "Frozen Foods",
        "Dry Goods",
        "Bulk Goods",
        "Health",
        "None",
        "Bakery"
    ]
}
