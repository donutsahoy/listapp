//
//  ShareCodeReturnModel.swift
//  BasketBuddy
//
//  Created by Robert J. Sarvis Jr on 4/7/23.
//

import Foundation

struct ShareCodeReturnModel: Codable {
    let shareCode: String
}
