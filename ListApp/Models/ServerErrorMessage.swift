//
//  ServerErrorMessage.swift
//  BasketBuddy
//
//  Created by Robert J. Sarvis Jr on 4/7/23.
//

import Foundation

struct ServerErrorMessage: Codable {
    let error: String
}
