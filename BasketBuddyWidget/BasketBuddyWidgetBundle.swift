//
//  BasketBuddyWidgetBundle.swift
//  BasketBuddyWidget
//
//  Created by Robert J. Sarvis Jr on 11/8/23.
//

import WidgetKit
import SwiftUI

@main
struct BasketBuddyWidgetBundle: WidgetBundle {
    var body: some Widget {
        BasketBuddyWidget()
        BasketBuddyWidgetLiveActivity()
    }
}
